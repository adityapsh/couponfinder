<?php

// respone function: in json formate with info(status n message) and data part
function create_respone($status, $message, $data){
	if(is_null($data) || empty($data)){
		$json = json_encode(array(
			"info" => array(
				"status" => $status,
				"message" => $message,
			)
		));
	}	
	else {
		$json = json_encode(array(
			"info" => array(
				"status" => $status,
				"message" => $message,
			),
			"data" => array(
				"data" => $data,
			)
		));
	}	
}
?>