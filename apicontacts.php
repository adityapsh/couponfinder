<?php
class ApiConstants
{
    public static $_BASE_URL = '/';
    public static $_LOGIN_URL = '/login.php';
    public static $_REGISTER_CUST_URL = '/register_customer.php';
    public static $_FORGOT_PASS_URL = '/forgot_pass.php';
}
?>